# bridges-grafana

generating self signed certificate:
```bash
mkdir -p /etc/pki/public
mkdir -p /etc/pki/private
openssl req -x509 -newkey rsa:4096 -keyout /etc/pki/private/server.key -out /etc/pki/public/server.cer -days 3650 -nodes
```
