#!/bin/bash

docker stop grafana
docker rm grafana
docker run -d --network host --name=grafana -p 3000:3000 -v /opt/collector/bridges-grafana/files/datasources.yaml:/etc/grafana/provisioning/datasources/datasources.yaml -v /opt/collector/bridges-grafana/files/dashboards.yaml:/etc/grafana/provisioning/dashboards/dashboards.yaml -v /opt/collector/bridges-grafana/files/network-devices-dashboard.yaml:/dashboards/network/network-devices-dashboard.json -v /opt/collector/bridges-grafana/files/network-devices-snmp-dashboard.yaml:/dashboards/network/network-devices-dashboard-snmp.json -e GF_SECURITY_ADMIN_USER="admin" -e GF_SECURITY_ADMIN_PASSWORD="bridges" -e GF_AUTH_ANONYMOUS_ENABLED="true" grafana/grafana:7.0.3

docker stop grafana-proxy
docker rm grafana-proxy
docker run -d --network host --name=grafana-proxy -p 4000:4000 -v /etc/pki:/etc/pki -v /opt/collector/bridges-grafana/files/nginx.conf:/etc/nginx/conf.d/default.conf:ro nginx:latest
